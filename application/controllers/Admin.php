<?php
class admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
	}

	function do_upload($pagelink)
	{

		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile')) {
			$imgdata = array('upload_data' => $this->upload->data());

			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			redirect('admin/'.$pagelink);
		}

		return $imgname;
	}

	public function index()
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['orders'] = $this->admin_model->get_orders();
		$data['users'] = $this->admin_model->get_users_status();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/index.php', $data);
		$this->load->view('templates/users/footer.php');
	}


	public function login()
	{
		if ($this->session->admindata('food_admin_id')) {
			redirect('admin');
		}

		$data['title'] = 'Foodhawk = Login';
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/admin/login.php', $data);
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$admin_id = $this->admin_model->login($email, $password);


			// echo $admin_id;
			// die;
			//$employee_id = $this->employee_model->login($email, $password);

			if ($admin_id) {
				$admin_data = array(
					'food_admin_id' => $admin_id,
					'email' => $email,
					'apna_logged_in' => true
				);
				$this->session->set_admindata($admin_data);


				//set cookie for 1 year
				$cookie = array(
					'name'   => 'food_admin_id',
					'value'  => $admin_id,
					'expire' => time() + 31556926
				);
				$this->input->set_cookie($cookie);


				$this->session->set_flashdata('admin_loggedin', 'You are now logged in');
				redirect('admin/login');
			} else {

				$this->session->set_flashdata('login_failed', 'Credentials  is invalid. Incorrect username or password.');
				redirect('admin/login');
			}
		}
	}

	public function logout()
	{

		$this->session->unset_userdata('food_admin_id');
		$this->session->unset_userdata('email');

		delete_cookie('food_admin_id');

		$this->session->set_flashdata('admin_loggedout', 'You are now logged out');
		redirect('admin/login');
	}

	public function forgetpassword()
	{
		if ($this->session->admindata('food_admin_id')) {
			redirect('admin');
		}
		$this->form_validation->set_rules('email', 'email', 'required');

		if ($this->form_validation->run() === FALSE) {
			$s_n = '3';
			$data = $s_n;
			$this->load->view('templates/users/header.php');
			$this->load->view('templates/admin/forgetpassword.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {
			$email = $this->input->post('email');
			$rand = mt_rand();

			$this->admin_model->forgetpassword($email, $rand);


			$this->load->view('templates/users/header.php');
			$this->load->view('templates/admin/conformcode.php');
			$this->load->view('templates/admin/footer.php');
		}
	}

	public function conformation()
	{
		$this->form_validation->set_rules('admin_key', 'admin_key', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('empty', 'Please enter a Valid Code.');
			redirect('admin/forgetpassword');
		} else {
			$admin_key = $this->input->post('admin_key');
			$admin_id  = $this->admin_model->admindetail($admin_key);

			if ($admin_id) {
				$adminid = $admin_id['admin_id'];
				$data['admindetail'] = $this->admin_model->get_admin($adminid);
				$this->load->view('templates/users/header.php');
				$this->load->view('templates/admin/newpassword.php', $data);
				$this->load->view('templates/admin/footer.php');
			} else {
				$this->session->set_flashdata('change_failed', ' You entered a wrong verification code.');
				redirect('admin/conformation');
			}
		}
	}
	public function newpassword()
	{

		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');


		if ($this->form_validation->run() === FALSE) {
			redirect('admin/forgetpassword');
		} else {
			$email = $this->input->post('email');
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$savepass = $this->admin_model->updatepassword($enc_password, $email);
			if (!$savepass) {
				$this->session->set_flashdata('password_changed', 'Your password has been changed successfully! Thank you');
				redirect('admin/login');
			} else {
				$this->session->set_flashdata('error', 'Error in Password Change.');
				redirect('admin/forgetpassword');
			}
		}
	}



	public function adduser()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('mobile', 'mobile', 'required');


		if ($this->form_validation->run() === FALSE) {

			$data['countries'] = $this->admin_model->get_active_country();
			$data['cities'] = $this->admin_model->get_cities();
			$data['zipcodes'] = $this->admin_model->get_zipcodes();
			$this->load->view('templates/users/header.php');
			$this->load->view('templates/admin/navbar.php');
			$this->load->view('templates/admin/aside.php');
			$this->load->view('templates/admin/adduser.php');
			$this->load->view('templates/users/footer.php');
		} else {
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$imgname = $this->do_upload("adduser");
			$userid	= $this->user_model->register($enc_password, $imgname);

			$this->user_model->addaddress($userid);
			$this->session->set_flashdata('user_registered', 'You are successfully registered');

			redirect('admin/manageusers');
		}
	}

	public function manageusers()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$data['profiles'] = $this->user_model->get_profiles();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/manageprofile.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function editprofile($user_id)
	{
		$data['countries'] = $this->admin_model->get_active_country();

		$data['cities'] = $this->admin_model->get_cities();
		$data['zipcodes'] = $this->admin_model->get_zipcodes();

		$data['profile'] = $this->user_model->get_profile($user_id);
		$data['addresss'] = $this->user_model->get_address($user_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/editprofile.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function updateprofile()
	{

		$user_id = $this->input->post('user_id');
		$this->user_model->update_profile($user_id);

		redirect('admin/manageusers');
	}

	public function deleteprofile($profileid)
	{
		$this->admin_model->deleteuser($profileid);
		redirect('admin/manageusers');
	}

	public function addadmin()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[admin.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('mobile', 'mobile', 'required');


		if ($this->form_validation->run() === FALSE) {

			$data['countries'] = $this->admin_model->get_active_country();

			$data['cities'] = $this->admin_model->get_cities();
			$data['zipcodes'] = $this->admin_model->get_zipcodes();

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/admin/navbar.php');
			$this->load->view('templates/admin/aside.php');
			$this->load->view('templates/admin/addadmin.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$imgname = $this->do_upload('addadmin');
			$this->admin_model->addadmin($enc_password, $imgname);
			$this->session->set_flashdata('admin_registered', 'You are successfully registered');

			redirect('admin/addadmin');
		}
	}

	public function manageadmins()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$data['admins'] = $this->admin_model->get_admins();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/manageadmin.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function ajax_user_enable($user_id)
	{
		$this->admin_model->enableuser($user_id);
		redirect('admin/manageusers');
	}
	public function ajax_user_disable($user_id)
	{
		$this->admin_model->disableuser($user_id);
		redirect('admin/manageusers');
	}


	public function ajax_admin_enable($admin_id)
	{
		$this->admin_model->enableadmin($admin_id);
		redirect('admin/manageadmins');
	}
	public function ajax_admin_disable($admin_id)
	{
		$this->admin_model->disableadmin($admin_id);
		redirect('admin/manageadmins');
	}


	public function deleteadmin($adminid)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->deleteadmin($adminid);
		redirect('admin/manageadmins');
	}

	public function profile()
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$admin_id	= $this->session->admindata('food_admin_id');
		$data['admin'] = $this->admin_model->get_admin($admin_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/admin.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function viewadmin($admin_id)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['admin'] = $this->admin_model->get_admin($admin_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/admin.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function editadmin($user_id)
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['admin'] = $this->admin_model->get_admin($user_id);
		$data['countries'] = $this->admin_model->get_active_country();

		$data['cities'] = $this->admin_model->get_cities();
		$data['zipcodes'] = $this->admin_model->get_zipcodes();
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/editadmin.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function adminimage()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$admin_id = $this->input->post('admin_id');
		$imgname = $this->do_upload('adminimage');
		$this->admin_model->adminimage($imgname, $admin_id);
		redirect('admin/viewadmin/' . $admin_id);
	}

	public function updateadmin()
	{

		$admin_id = $this->input->post('admin_id');
		$this->admin_model->update_admin($admin_id);

		redirect('admin/viewadmin/' . $admin_id);
	}

	public function addorder()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/order.php');
		$this->load->view('templates/users/footer.php');
	}

	public function manageorder()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$data['orders'] = $this->admin_model->get_orders();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/manageorder.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function approved($orderid, $pagelink)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->approved($orderid);
		$order = $this->admin_model->get_order($orderid);
		$userid = $order['order_user_id'];


		$this->admin_model->approvednotification($userid, $orderid);

		if ($pagelink == "Pendingapproval") {
			redirect('admin/Pendingapproval');
		} elseif ($pagelink == "readyfordelivered") {

			redirect('admin/readyfordelivered');
		} elseif ($pagelink == "delivery") {
			redirect('admin/delivery');
		} else {
			redirect('admin/manageorder');
		}
	}

	public function rejected($orderid, $pagelink)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->admin_model->rejected($orderid);
		$order = $this->admin_model->get_order($orderid);
		$userid = $order['order_user_id'];
		$this->admin_model->rejectednotification($userid, $orderid);
		if ($pagelink == "Pendingapproval") {
			redirect('admin/Pendingapproval');
		} elseif ($pagelink == "readyfordelivered") {

			redirect('admin/readyfordelivered');
		} elseif ($pagelink == "delivery") {
			redirect('admin/delivery');
		} else {
			redirect('admin/manageorder');
		}
	}

	public function cancelled($orderid, $pagelink)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->cancelled($orderid);
		$order = $this->admin_model->get_order($orderid);
		$userid = $order['order_user_id'];
		$this->admin_model->cancellednotification($userid, $orderid);
		if ($pagelink == "Pendingapproval") {
			redirect('admin/Pendingapproval');
		} elseif ($pagelink == "readyfordelivered") {

			redirect('admin/readyfordelivered');
		} elseif ($pagelink == "delivery") {
			redirect('admin/delivery');
		} else {
			redirect('admin/manageorder');
		}
	}

	public function readyfordelivery($orderid, $pagelink)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->readyfordelivery($orderid);
		$order = $this->admin_model->get_order($orderid);
		$userid = $order['order_user_id'];
		$this->admin_model->readyfordeliverynotification($userid, $orderid);
		if ($pagelink == "Pendingapproval") {
			redirect('admin/Pendingapproval');
		} elseif ($pagelink == "readyfordelivered") {

			redirect('admin/readyfordelivered');
		} elseif ($pagelink == "delivery") {
			redirect('admin/delivery');
		} else {
			redirect('admin/manageorder');
		}
	}

	public function delivered($orderid, $pagelink)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->delivered($orderid);
		$order = $this->admin_model->get_order($orderid);
		$userid = $order['order_user_id'];
		$this->admin_model->deliverednotification($userid, $orderid);
		if ($pagelink == "Pendingapproval") {
			redirect('admin/Pendingapproval');
		} elseif ($pagelink == "readyfordelivered") {

			redirect('admin/readyfordelivered');
		} elseif ($pagelink == "delivery") {
			redirect('admin/delivery');
		} else {
			redirect('admin/manageorder');
		}
	}

	public function Pendingapproval()
	{



		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['orders'] = $this->admin_model->get_Pendingapproval();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/pendingapproval.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function readyfordelivered()
	{



		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['orders'] = $this->admin_model->get_readyfordelivery();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/rfrdel', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function delivery()
	{


		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['orders'] = $this->admin_model->get_delivery();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/deliverd.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function report()
	{


		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['orders'] = $this->admin_model->get_orders();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/report.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function notifications()
	{


		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$user_id = $this->session->userdata('food_admin_id');
		$data['notifications'] = $this->admin_model->get_notification($user_id);
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/notifications.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function diet()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('diet_price', 'diet_price', 'required');
		$this->form_validation->set_rules('diet_name', 'diet_name', 'required|is_unique[diet.diet_name]');
		if ($this->form_validation->run() === FALSE) {
			redirect('admin/setting');
		} else {
			$this->admin_model->adddiet();

			redirect('admin/setting');
		}
	}

	public function updatediet()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('diet_price', 'diet_price', 'required');
		$this->form_validation->set_rules('diet_name', 'diet_name', 'required');
		if ($this->form_validation->run() === FALSE) {
		} else {
			$dietid = $this->input->post('diet_id');
			$this->admin_model->updatediet($dietid);

			redirect('admin/setting');
		}
	}

	public function deletediet($dietid)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->deletediet($dietid);
		redirect('admin/setting');
	}
	public function category()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('category_price', 'category_price', 'required');
		$this->form_validation->set_rules('category_name', 'category_name', 'required|is_unique[category.category_name]');
		if ($this->form_validation->run() === FALSE) {
			redirect('admin/setting');
		} else {
			$this->admin_model->addcategory();

			redirect('admin/category');
		}
	}

	public function updatecategory()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('category_price', 'category_price', 'required');
		$this->form_validation->set_rules('category_name', 'category_name', 'required|is_unique[category.category_name]');
		if ($this->form_validation->run() === FALSE) {
		} else {
			$categoryid = $this->input->post('category_id');
			$this->admin_model->updatecategory($categoryid);

			redirect('admin/setting');
		}
	}

	public function deletecategory($categoryid)
	{
		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->deletecategory($categoryid);
		redirect('admin/setting');
	}
	public function country()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('country_name', 'country_name', 'required|is_unique[countries.country_name]');
		if ($this->form_validation->run() === FALSE) {
		} else {
			$this->admin_model->addcountry();

			redirect('admin/countrysetup');
		}
	}
	public function city()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('country_city_id', 'country_city_id', 'required');
		$this->form_validation->set_rules('city_name', 'city_name', 'required|is_unique[cities.city_name]');
		if ($this->form_validation->run() === FALSE) {
		} else {
			$this->admin_model->addcity();

			redirect('admin/countrysetup');
		}
	}

	public function zipcode()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}

		$this->form_validation->set_rules('zipcode', 'zipcode', 'required|is_unique[zipcode.zipcode]');
		if ($this->form_validation->run() === FALSE) {
			redirect('admin/setting');
		} else {
			$this->admin_model->addzipcode();

			redirect('admin/setting');
		}
	}




	// setting
	public function setting()
	{

		if (!$this->session->admindata('food_admin_id')) {
			redirect('admin/login');
		}
		$data['diets'] = $this->admin_model->get_diet();
		$data['categories'] = $this->admin_model->get_category();
		$data['guests'] = $this->admin_model->guest();
		$data['zipcodes'] = $this->admin_model->get_zipcodes();
		$data['countries'] = $this->admin_model->get_countries_deactivate();
		$data['cities'] = $this->admin_model->get_cities_deactivate();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/settings.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function updateguest()
	{
		$this->form_validation->set_rules('guest', 'guest', 'required|numeric');
		if ($this->form_validation->run() === FALSE) {
			redirect('admin/countrysetup?=evr');
		} else {
			$this->admin_model->updateguest();

			redirect('admin/setting');
		}
	}

	// ajax

	public function ajax_notification_read($notificationid)
	{
		$this->user_model->notificationread($notificationid);
	}
	public function ajax_notification_unread($notificationid)
	{
		$this->user_model->notificationunread($notificationid);
	}
	public function ajax_edit_diet($dietid)
	{
		$data['diet'] =	 $this->admin_model->ajax_edit_diet($dietid);
		$this->load->view('templates/ajax/edit_diet.php', $data);
	}
	public function ajax_edit_category($categoryid)
	{
		$data['category'] =	 $this->admin_model->ajax_edit_category($categoryid);
		$this->load->view('templates/ajax/edit_catagory.php', $data);
	}
	public function ajax_edit_country($countryid)
	{
		$data['country'] =	 $this->admin_model->get_country($countryid);
		$this->load->view('templates/ajax/editcountry.php', $data);
	}
	public function ajax_edit_city($cityid)
	{
		$data['countries'] = $this->admin_model->get_countries();
		$data['city'] =	 $this->admin_model->get_city($cityid);
		$this->load->view('templates/ajax/editcity.php', $data);
	}
	public function ajax_edit_state($stateid)
	{
		$data['countries'] = $this->admin_model->get_countries();
		$data['cities'] = $this->admin_model->get_cities();
		$data['state'] =	 $this->admin_model->get_state($stateid);
		$this->load->view('templates/ajax/editstate.php', $data);
	}
	public function ajax_edit_zipcode($zipcodeid)
	{
		$data['zipcode'] =	 $this->admin_model->get_zipcode($zipcodeid);
		$this->load->view('templates/ajax/editzipcode.php', $data);
	}
}
