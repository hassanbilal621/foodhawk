<?php
class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	function do_upload($pagelink)
	{

		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile')) {
			$imgdata = array('upload_data' => $this->upload->data());

			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			redirect('admin/'.$pagelink);
		}

		return $imgname;
	}

	public function index()
	{
		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}
		$data['orders'] = $this->admin_model->get_orders();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/index.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function error404()
	{
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/404.php');
	}

	/////////*********************login Start*********************//////////

	public function login()
	{
		if ($this->session->userdata('food_user_id')) {
			redirect('users');
		}

		$data['title'] = 'Foodhawk = Login';
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/login.php', $data);
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$user_id = $this->user_model->login($email, $password);



			if ($user_id) {
				$currUser = $this->user_model->get_profile($user_id);
				$username = $currUser["name"];
				$photo = $currUser["profile_image"];

				$user_data = array(
					'food_user_id' => $user_id,
					'food_user_name' => $username,
					'food_user_photo' => $photo,
					'email' => $email,
					'food_logged_in' => true
				);
				$this->session->set_userdata($user_data);


				$this->session->set_flashdata('user_loggedin', 'You are now logged in');
				redirect('users/login');
			} else {

				$this->session->set_flashdata('login_failed', ' Credentials  is invalid. Incorrect username or password.');
				redirect('users/login');
			}
		}
	}

	public function logout()
	{

		$this->session->unset_userdata('food_user_id');
		$this->session->unset_userdata('email');

		delete_cookie('food_user_id');

		$this->session->set_flashdata('user_loggedout', 'You are now logged out');
		redirect('users/login');
	}

	/////////*********************login End*********************//////////

	/////////*********************profile Start*********************//////////

	public function register()
	{


		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('mobile', 'mobile', 'required');


		if ($this->form_validation->run() === FALSE) {
			$data['countries'] = $this->admin_model->get_active_country();

			$data['cities'] = $this->admin_model->get_cities();
			$data['zipcodes'] = $this->admin_model->get_zipcodes();


			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/regist.php', $data);
		} else {
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$imgname = $this->do_upload('register');
			$this->user_model->register($enc_password, $imgname);
			$this->session->set_flashdata('user_registered', 'You are successfully registered');

			redirect('users/login');
		}
	}

	public function profile()
	{

		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}
		$user_id =	$this->session->userdata('food_user_id');
		$data['profile'] = $this->user_model->get_profile($user_id);
		$data['addresss'] = $this->user_model->get_address($user_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/profile.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function editprofile($user_id)
	{

		$data['profile'] = $this->user_model->get_profile($user_id);
		$data['addresss'] = $this->user_model->get_address($user_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/editprofile.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function profileimage()
	{
		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}
		$imgname = $this->do_upload('profileimage');
		$this->user_model->profileimage($imgname);
		redirect('users/profile');
	}

	public function updateprofile()
	{

		$user_id = $this->input->post('user_id');
		$this->user_model->update_profile($user_id);

		redirect('users/profile');
	}
	public function updateaddress()
	{

		$addressid = $this->input->post('addressid');
		$this->user_model->updateaddress($addressid);

		redirect('users/profile');
	}
	public function addaddress()
	{

		$user_id = $this->input->post('addressid');
		$this->user_model->addaddress($user_id);

		redirect('users/profile');
	}

	public function deleteaddress($addressid)
	{
		$this->user_model->deleteaddress($addressid);
	}

	public function forgetpassword()
	{
		if ($this->session->userdata('food_user_id')) {
			redirect('users');
		}
		$this->form_validation->set_rules('email', 'email', 'required');

		if ($this->form_validation->run() === FALSE) {
			$s_n = '3';
			$data = $s_n;
			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/forgetpassword.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {
			$email = $this->input->post('email');
			$rand = mt_rand();

			$this->user_model->forgetpassword($email, $rand);


			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/conformcode.php');
			$this->load->view('templates/users/footer.php');
		}
	}

	public function conformation()
	{
		$this->form_validation->set_rules('users_key', 'users_key', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('empty', 'Please enter a Valid Code.');
			redirect('users/forgetpassword');
		} else {
			$users_key = $this->input->post('users_key');
			$user_id  = $this->user_model->usersdetail($users_key);

			if ($user_id) {
				$userid = $user_id['user_id'];
				$data['usersdetail'] = $this->user_model->get_user($userid);
				$this->load->view('templates/users/header.php');
				$this->load->view('templates/users/newpassword.php', $data);
				$this->load->view('templates/users/footer.php');
			} else {
				$this->session->set_flashdata('change_failed', ' You entered a wrong verification code.');
				redirect('users/conformation');
			}
		}
	}
	public function newpassword()
	{

		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');


		if ($this->form_validation->run() === FALSE) {
			redirect('users/forgetpassword');
		} else {
			$email = $this->input->post('email');
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$savepass = $this->user_model->updatepassword($enc_password, $email);
			if ($savepass) {
				$this->session->set_flashdata('password_changed', 'Your password has been changed successfully! Thank you');
				redirect('users/login');
			} else {
				$this->session->set_flashdata('error', 'Error in Password Change.');
				redirect('users/forgetpassword');
			}
		}
	}



	/////////*********************profile End*********************//////////

	/////////*********************Pages Start*********************//////////

	public function myfavorites()
	{
		$data['orders'] = $this->user_model->get_order_favorites();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/myfavorites.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function notification()
	{

		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}

		$user_id = $this->session->userdata('food_user_id');
		$data['notifications'] = $this->user_model->get_notification($user_id);
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/notification.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function notificationread($notificationid)
	{
		$this->user_model->notificationread($notificationid);
		redirect('users/notification');
	}


	public function orderhistory()
	{

		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}
		$user_id = $this->session->userdata('food_user_id');
		$data['orders'] = $this->admin_model->get_orders_userid($user_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/orderhistory.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function orderfood()
	{

		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}



		$user_id = $this->session->userdata('food_user_id');
		$data['guests'] = $this->admin_model->guest();
		$data['products'] = $this->user_model->get_products();
		$data['addresss'] = $this->user_model->get_address($user_id);
		$data['zipcodes'] = $this->admin_model->get_zipcodes();
		
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/orderfood.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function speedorder()
	{

		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('time', 'time', 'required');
		// $this->form_validation->set_rules('food', 'food', 'required');


		if ($this->form_validation->run() === FALSE) {
			redirect('users/orderfood');
		} else {



			$user_id = $this->session->userdata('food_user_id');



			$orderid = $this->user_model->orderfood($user_id);



			$this->admin_model->pendingnotification($orderid, $user_id);





			$number = count($_POST["diet_id"]);
			if ($number > 1) {
				for ($i = 0; $i < $number; $i++) {
					if (trim($_POST["diet_id"][$i] != '')) {
						$this->orderdietitem($orderid, $_POST["diet_id"][$i], $_POST["dietnumber"][$i], $_POST["dietnote"][$i]);
					}
				}
			}

			$number = count($_POST["food"]);
			if ($number > 1) {
				for ($i = 0; $i < $number; $i++) {
					if (trim($_POST["food"][$i] != '')) {
						$this->orderfooditem($orderid, $_POST["food"][$i]);
					}
				}
			}
			$number = count($_POST["desert"]);
			if ($number > 1) {
				for ($i = 0; $i < $number; $i++) {
					if (trim($_POST["desert"][$i] != '')) {
						$this->orderdesertitem($orderid, $_POST["desert"][$i]);
					}
				}
			}

			redirect('users/orderhistory');
		}
	}

	public function favorites($orderid, $pagelink)
	{

		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}


		$this->user_model->favorites($orderid);

		if ($pagelink == "fav") {
			redirect('users/myfavorites');

		} elseif ($pagelink == "history") {

			redirect('users/index');

		} else {
			redirect('users/orderhistory');
			
		}
	}

	public function removefavorites($orderid, $pagelink)
	{
		if (!$this->session->userdata('food_user_id')) {
			redirect('users/login');
		}
		$this->user_model->removefavorites($orderid);

		if ($pagelink == "fav") {
			redirect('users/myfavorites');

		} elseif ($pagelink == "history") {

			redirect('users/index');
			
		} else {
			redirect('users/orderhistory');
			
		}
	}


	public function orderdietitem($orderid, $diet_id, $dietnumber, $dietnote)
	{

		$data = array(
			'diet_id' => $diet_id,
			'diet_num' => $dietnumber,
			'diet_note' => $dietnote,
			'order_id_diet' => $orderid,
		);

		$this->security->xss_clean($data);
		$this->db->insert('order_item_diet', $data);
	}

	public function orderdesertitem($orderid, $desert)
	{

		$data = array(

			'order_desert' => $desert,
			'order_id_desert' => $orderid,
		);

		$this->security->xss_clean($data);
		$this->db->insert('order_desert_item', $data);
	}

	public function orderfooditem($orderid, $food)
	{

		$data = array(
			'order_food' => $food,
			'order_id_food' => $orderid,
		);

		$this->security->xss_clean($data);
		$this->db->insert('order_food_item', $data);
	}



	/////////*********************Pages End*********************//////////

	/////////*********************ajax Start*********************//////////


	public function ajax_get_product_details($productid)
	{
		$myObj =  $this->user_model->get_ajax_product($productid);
		$myJSON = json_encode($myObj);
		echo $myJSON;
	}
	public function ajax_get_address_details($addressid)
	{
		$myObj =  $this->user_model->get_ajax_address($addressid);
		$myJSON = json_encode($myObj);
		echo $myJSON;
	}
	public function ajax_edit_address_details($addressid)
	{
		$data['address'] =	 $this->user_model->get_ajax_address($addressid);
		$this->load->view('templates/ajax/editaddress.php', $data);
	}
	public function ajax_add_address_details($user_id)
	{
		$data['profile'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/ajax/addaddress.php', $data);
	}
	/////////*********************ajax End*********************//////////

}
