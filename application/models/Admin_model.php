<?php
class Admin_model extends CI_Model
{
	public function login($email, $password)
	{
		$this->db->where('email', $email);
		$result = $this->db->get('admin');
		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;
			if (password_verify($password, $hash)) {
				return $result->row(0)->admin_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function forgetpassword($email, $rand)
	{
		$data = array(
			'admin_key' => $rand

		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('admin', $data);
	}
	public function admindetail($admin_key)
	{
		$this->db->where('admin_key', $admin_key);
		$query = $this->db->get('admin');
		return $query->row_array();
	}
	public function updatepassword($enc_password, $email)
	{
		$data = array(
			'password' => $enc_password,
			'admin_key' => 'null'

		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('admin', $data);
	}

	// admin

	public function get_admin($adminid)
	{
		$this->db->where('admin_id', $adminid);
		$query = $this->db->get('admin');
		return $query->row_array();
	}

	public function get_admins()
	{
		$query = $this->db->get('admin');
		return $query->result_array();
	}
	public function deleteadmin($adminid)
	{
		$this->db->where('admin_id', $adminid);
		$this->db->delete('admin');
	}
	public function addadmin($enc_password, $imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'mobile' => $this->input->post('mobile'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'role' => $this->input->post('role'),
			'password' => $enc_password,
			'corporate_application' => $imgname,
			'profile_image' => 'no_image.jpg',
			'status' => 'deactivate',
			'registration_date' => date('d/m/Y')
		);
		$this->security->xss_clean($data);
		return $this->db->insert('admin', $data);
	}
	public function adminimage($imgname, $admin_id)
	{
		$data = array(
			'profile_image' => $imgname
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}
	function update_admin($admin_id)
	{
		$data = array(
			'name' => $this->input->post('name'),
			// 'email' => $this->input->post('email'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'mobile' => $this->input->post('mobile'),
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}

	public function enableuser($user_id)
	{
		$data = array(
			'status' => 'active'
		
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}
	public function disableuser($user_id)
	{
		$data = array(
			'status' => 'deactivate'
		
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}
	
	public function enableadmin($admin_id)
	{
		$data = array(
			'status' => 'active'
		
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}
	public function disableadmin($admin_id)
	{
		$data = array(
			'status' => 'deactivate'
		
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}
	public function deleteuser($userid)
	{
		$this->db->where('user_id', $userid);
		$this->db->delete('users');
	}


	// orders

	public function get_orders()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}

	public function pendingnotification($orderid, $userid)
	{
		
		$data = array(
			'notification_title' => 'Your order status set to pending, your Order Id: #' .$orderid,
			'notification' => 'Your order status set to pending Order Id: #' .$orderid,
			'notification_user_id' => $userid,
			'admin_user' => '0',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'

		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function approved($orderid)
	{

		$data = array(
			'order_status' => 'approved'

		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}
	public function approvednotification($userid, $orderid)
	{
		
		$data = array(
			'notification_title' => 'Your order has been approved, your Order Id: #' .$orderid,

			'notification' => 'Your order has been approved, your Order Id: #' .$orderid,
			'notification_user_id' => $userid,
			'admin_user' => '0',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'

		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function rejected($orderid)
	{
		$data = array(
			'order_status' => 'rejected'

		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);

	}
	public function rejectednotification($userid, $orderid)
	{
		
		$data = array(
			'notification_title' => 'Your Order Has Been Rejected Order ID' .$orderid,
			'notification' => 'Your Order Has Been Rejected Order ID : ' .$orderid,
			'notification_user_id' => $userid,
			'admin_user' => '0',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'

		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function cancelled($orderid)
	{
		$data = array(
			'order_status' => 'cancelled'

		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);

	}
	public function cancellednotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Cancelled Order Id' .$orderid,
			'notification' => 'Your Order Has Been Cancelled Order Id' .$orderid,
			'notification_user_id' => $userid,
			'admin_user' => '0',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'

		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function readyfordelivery($orderid)
	{
		$data = array(
			'order_status' => 'readyfordelivery'

		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);

	}
	public function readyfordeliverynotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Ready For Delivery order ID' .$orderid,
			'notification' => 'Your Order Has Been Ready For Delivery order ID' .$orderid,
			'notification_user_id' => $userid,
			'admin_user' => '0',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'

		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function delivered($orderid)
	{

		$data = array(
			'order_status' => 'delivered'

		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}

	public function deliverednotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Delivered order ID' .$orderid,
			'notification' => 'Your Order Has Been Delivered order ID' .$orderid,
			'notification_user_id' => $userid,
			'admin_user' => '0',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'

		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}





	public function get_delivery()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.order_status', 'delivered');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}

	public function get_Pendingapproval()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.order_status', 'pendingapproval');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}

	public function get_readyfordelivery()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.order_status', 'readyfordelivery');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_orders_userid($user_id)
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_user_id', $user_id);
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_order($order_id)
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_id', $order_id);
		$query = $this->db->get('order_food');
		return $query->row_array();
	}

	// user?

	public function get_users_status()
	{
		$this->db->where('status', 'deactivate');
		$query = $this->db->get('users');
		return $query->result_array();
	}


	// get_notification

	public function get_notification()
	{
		$this->db->where('admin_user', '1');
		$query = $this->db->get('notification');
		return $query->result_array();
	}

	// diet
	public function adddiet()
	{
		$data = array(
			'diet_name' => $this->input->post('diet_name'),
			'diet_price' => $this->input->post('diet_price'),
		);
		$this->security->xss_clean($data);
		return $this->db->insert('diet', $data);
	}
	public function updatediet($dietid)
	{
		$data = array(
			'diet_name' => $this->input->post('diet_name'),
			'diet_price' => $this->input->post('diet_price'),
		);
		$this->security->xss_clean($data);
		$this->db->where('diet_id', $dietid);
		return $this->db->update('diet', $data);
	}
	public function deletediet($dietid)
	{
		$this->db->where('diet_id', $dietid);
		$this->db->delete('diet');
	}

	public function get_diet()
	{
		$query = $this->db->get('diet');
		return $query->result_array();
	}


	// country
	public function get_countries()
	{
		$query = $this->db->get('countries');
		return $query->result_array();
	}
	public function get_country($countryid)
	{
		$this->db->where('country_id', $countryid);
		$query = $this->db->get('countries');
		return $query->row_array();
	}

	public function get_active_country()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('countries');
		return $query->result_array();
	}
	public function get_countries_deactivate()
	{
		$this->db->where('status', 'deactivate');
		$query = $this->db->get('countries');
		return $query->result_array();
	}
	public function addcountry()
	{
		$data = array(
			'country_name' => $this->input->post('country_name'),
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('countries', $data);
	}
	public function activecountry($countryid)
	{
		$data = array(
			'status' => 'active'

		);
		$this->security->xss_clean($data);
		$this->db->where('country_id', $countryid);
		return $this->db->update('countries', $data);
	}

	public function deactivatecountry($countryid)
	{
		$data = array(
			'status' => 'deactivate'

		);
		$this->security->xss_clean($data);
		$this->db->where('country_id', $countryid);
		return $this->db->update('countries', $data);
	}


	// city


	public function get_cities()
	{
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_cities_deactivate()
	{
		$this->db->where('status', 'deactivate');
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_cities_active()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_city($cityid)
	{
		$this->db->where('city_id', $cityid);
		$query = $this->db->get('cities');
		return $query->row_array();
	}

	public function addcity()
	{
		$data = array(
			'city_name' => $this->input->post('city_name'),
			'country_city_id' => $this->input->post('country_city_id'),
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('cities', $data);
	}
	public function updatecity($cityid)
	{
		$data = array(
			'city_name' => $this->input->post('city_name'),
			'country_city_id' => $this->input->post('country_city_id'),
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		$this->db->where('city_id', $cityid);
		return $this->db->update('cities', $data);
	}

	// countrysetup

	public function countrysetup()
	{
		$this->db->join('cities', 'countries.country_id = cities.country_city_id', 'left');
		$this->db->join('states', 'cities.city_id = states.city_state_id', 'left');
		$query = $this->db->get('countries');
		return $query->result_array();
	}


	// state

	public function get_states()
	{
		$query = $this->db->get('states');
		return $query->result_array();
	}
	public function get_state($stateid)
	{
		$this->db->where('state_id', $stateid);
		$query = $this->db->get('states');
		return $query->row_array();
	}
	public function addstate()
	{
		$data = array(
			'state_name' => $this->input->post('state_name'),
			'city_state_id' => $this->input->post('city_state_id'),
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('states', $data);
	}
	public function updatestate($stateid)
	{
		$data = array(
			'state_name' => $this->input->post('state_name'),
			'city_state_id' => $this->input->post('city_state_id'),
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		$this->db->where('state_id', $stateid);
		return $this->db->update('states', $data);
	}
	public function deletestate($stateid)
	{
		$this->db->where('state_id', $stateid);
		$this->db->delete('states');
	}

	// category?

	public function addcategory()
	{
		$data = array(
			'category_name' => $this->input->post('category_name'),
			'category_price' => $this->input->post('category_price'),
		);
		$this->security->xss_clean($data);
		return $this->db->insert('category', $data);
	}
	public function updatecategory($categoryid)
	{
		$data = array(
			'category_name' => $this->input->post('category_name'),
			'category_price' => $this->input->post('category_price'),
		);
		$this->security->xss_clean($data);
		$this->db->where('category_id', $categoryid);
		return $this->db->update('category', $data);
	}
	public function deletecategory($categoryid)
	{
		$this->db->where('category_id', $categoryid);
		$this->db->delete('category');
	}
	public function get_category()
	{
		$query = $this->db->get('category');
		return $query->result_array();
	}
	// zipcode/

	public function addzipcode()
	{
		$data = array(
			'zipcode' => $this->input->post('zipcode'),
		);
		$this->security->xss_clean($data);
		return $this->db->insert('zipcode', $data);
	}
	public function updatezipcode($zipcodeid)
	{
		$data = array(
			'zipcode' => $this->input->post('zipcode'),
		);
		$this->security->xss_clean($data);
		$this->db->where('zipcode_id', $zipcodeid);
		return $this->db->update('zipcode', $data);
	}
	public function deletezipcode($zipcodeid)
	{
		$this->db->where('zipcode_id', $zipcodeid);
		$this->db->delete('zipcode');
	}
	public function get_zipcode($zipcodeid)
	{
		$this->db->where('zipcode_id', $zipcodeid);
		$query = $this->db->get('zipcode');
		return $query->row_array();
	}
	public function get_zipcodes()
	{
		$query = $this->db->get('zipcode');
		return $query->result_array();
	}

	// settings

	public function guest()
	{
		$this->db->where('id', '1');
		$query = $this->db->get('settings');
		return $query->row_array();
	}
	public function updateguest()
	{
		$data = array(
			'guest' => $this->input->post('guest'),

		);
		$this->security->xss_clean($data);
		$this->db->where('id', '1');
		return $this->db->update('settings', $data);
	}

	// ajax
	public function ajax_edit_category($categoryid)
	{
		$this->db->where('category_id', $categoryid);
		$query = $this->db->get('category');
		return $query->row_array();
	}
	public function ajax_edit_diet($dietid)
	{
		$this->db->where('diet_id', $dietid);
		$query = $this->db->get('diet');
		return $query->row_array();
	}
}
