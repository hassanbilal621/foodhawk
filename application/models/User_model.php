<?php
class User_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function login($email, $password)
	{

		$this->db->where('email', $email);
		$this->db->where('status', 'active');

		$result = $this->db->get('users');


		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->user_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function register($enc_password, $imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'mobile' => $this->input->post('mobile'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'credit_card' => $this->input->post('credit'),
			'profile_image' => 'no_image.jpg',
			'password' => $enc_password,
			'corporate_application' => $imgname,
			'status' => 'deactivate',
			'registration_date' => date('d/m/Y')
		);

		$this->security->xss_clean($data);
		 $this->db->insert('users', $data);
		return $this->db->insert_id();
	}

	public function favorites($orderid)
	{
		$data = array(
			'favorites' => 'yes',
		);

		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		$this->db->update('order_food', $data);
	}
	public function removefavorites($orderid)
	{
		$data = array(
			'favorites' => 'no',
		);

		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		$this->db->update('order_food', $data);
	}

	public function get_order_favorites()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.favorites', 'yes');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function profileimage($imgname)
	{
		$data = array(
			'profile_image' => $imgname

		);

		$this->security->xss_clean($data);
		$this->db->where('user_id', $this->input->post('user_id'));
		$this->db->update('users', $data);
	}
	public function get_profiles()
	{
		$query = $this->db->get('users');
		return $query->result_array();
	}
	public function update_profile($user_id)
	{
		$data = array(
			'name' => $this->input->post('name'),
			// 'email' => $this->input->post('email'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode' => $this->input->post('zipcode'),
			'credit_card' => $this->input->post('credit_card'),
			'name_of_organization' => $this->input->post('name_of_organization'),
			'cantact_name' => $this->input->post('cantact_name'),
			'phone_num' => $this->input->post('phone_num'),
			'mobile' => $this->input->post('mobile'),
			'delivery_address' => $this->input->post('delivery_address'),
			'account_note' => $this->input->post('account_note'),
			'delivery_instruction' => $this->input->post('delivery_instruction'),
			'diet_prefrence' => $this->input->post('diet_prefrence'),
			'allergies' => $this->input->post('allergies'),
			'budget_prefrence' => $this->input->post('budget_prefrence'),
			'day_Schedule' => $this->input->post('day_Schedule')

		);

		$this->security->xss_clean($data);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}

	public function orderfood($user_id)
	{


		if (!empty($this->input->post('oldaddress'))) {

			$address = $this->input->post('oldaddress');
			$zipcode = $this->input->post('oldzipcode');
		} else {

			$address = $this->input->post('newaddress');
			$zipcode = $this->input->post('newzipcode');
			$data = array(
				'address' => 	$address,
				'zipcode' => $zipcode,
				'address_user_id' => $user_id
			);

			$this->security->xss_clean($data);
			$this->db->insert('address', $data);
		}

		$data = array(
			'date' => $this->input->post('date'),
			'time' => $this->input->post('time'),
			//'food' => $this->input->post('food'),
			'guest' => $this->input->post('guest'),
			//'desert' => $this->input->post('desert'),
			'delivery_instruction' => $this->input->post('delivery_instruction'),
			'address' => $address,
			'zipcode' => $zipcode,
			'order_status' => 'pendingapproval',
			'order_user_id' => $user_id
		);

		$this->security->xss_clean($data);
		$this->db->insert('order_food', $data);

		return $this->db->insert_id();
	}

	public function get_user($user_id)
	{
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('users');
		return $query->row_array();
	}

	public function forgetpassword($email, $rand)
	{
		$data = array(
			'users_key' => $rand

		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('users', $data);
	}
	public function usersdetail($users_key)
	{
		$this->db->where('users_key', $users_key);
		$query = $this->db->get('users');
		return $query->row_array();
	}
	public function updatepassword($enc_password, $email)
	{
		$data = array(
			'password' => $enc_password,
			'users_key' => 'null'

		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('users', $data);
	}
	public function updateaddress($addressid)
	{
		$data = array(
			'address' => $this->input->post('address'),
			'zipcode' => $this->input->post('zipcode')

		);

		$this->security->xss_clean($data);
		$this->db->where('address_id', $addressid);
		$this->db->update('address', $data);
	}
	public function deleteaddress($addressid)
	{

		$this->db->where('address_id', $addressid);
		$this->db->delete('address');
	}

	public function addaddress($user_id)
	{
		$data = array(
			'address' => $this->input->post('address'),
			'zipcode' => $this->input->post('zipcode'),
			'address_user_id' => $user_id
		);

		$this->security->xss_clean($data);
		$this->db->insert('address', $data);
	}

	public function get_address($user_id)
	{
		$this->db->where('address_user_id', $user_id);
		$query = $this->db->get('address');
		return $query->result_array();
	}



	public function get_ajax_address($addressid)
	{
		$this->db->where('address_id', $addressid);
		$query = $this->db->get('address');
		return $query->row_array();
	}


	public function get_products()
	{

		$query = $this->db->get('food');
		return $query->result_array();
	}

	public function get_profile($profiledata)
	{

		$this->db->where('user_id', $profiledata);
		$query = $this->db->get('users');
		return $query->row_array();
	}

	public function get_notification($user_id)
	{
		$this->db->where('notification_user_id', $user_id);
		$this->db->where('admin_user', '0');
		$query = $this->db->get('notification');
		return $query->result_array();
	}

	public function notificationread($notificationid)
	{
		$data = array(
			'status' => 'read'
		);

		$this->security->xss_clean($data);
		$this->db->where('notification_id', $notificationid);
		$this->db->update('notification', $data);
	}
	public function notificationunread($notificationid)
	{
		$data = array(
			'status' => 'unread'
		);

		$this->security->xss_clean($data);
		$this->db->where('notification_id', $notificationid);
		$this->db->update('notification', $data);
	}

	public function get_ajax_notification($notification_id)
	{
		$this->db->where('notification_id', $notification_id);
		$query = $this->db->get('notification');
		return $query->row_array();
	}
	// ajax

	public function get_ajax_product($productid)
	{
		$this->db->where('food_id', $productid);
		$result = $this->db->get('food');
		return $result->row_array();
	}
}
