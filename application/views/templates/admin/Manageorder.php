<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">
                            <span<span style="font-weight: bold;">Manage Order</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <section class="users-list-wrapper section">
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <div class="responsive-table">
                                    <table id="page-length-option" class="display">
                                        <thead>
                                            <tr>
                                                <th>#Order ID</th>
                                                <th>Date & Time</th>
                                                <th>user Name</th>
                                                <th>Order Status</th>
                                                <th>Action</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $s_no = '1';
                                            foreach ($orders as $order) : ?>
                                                <tr>
                                                    <td>#<?php echo $order['order_id']; ?></td>
                                                    <td><?php echo $order['date']; ?> & <?php echo $order['time']; ?></td>
                                                    <td><?php echo $order['name']; ?></td>
                                                    <?php if ($order['order_status'] == 'approved') { ?>
                                                        <td class="chip green lighten-4">Approved </td>
                                                        <td>
                                                            <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                            <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>

                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/rejected/<?php echo $order['order_id']; ?>/manage" name="action">Rejected
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/cancelled/<?php echo $order['order_id']; ?>/manage">Cancelled
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/readyfordelivery/<?php echo $order['order_id']; ?>/manage">Ready For Delivery
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/delivered/<?php echo $order['order_id']; ?>/manage">Delivered
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <?php } elseif ($order['order_status'] == 'pendingapproval') { ?>
                                                        <td class="chip lighten-3 orange">Pending Approval</td>
                                                        <td>
                                                            <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                            <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/approved/<?php echo $order['order_id']; ?>/manage" type="submit" name="action">Approved
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/rejected/<?php echo $order['order_id']; ?>/manage" name="action">Rejected
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/cancelled/<?php echo $order['order_id']; ?>/manage">Cancelled
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/readyfordelivery/<?php echo $order['order_id']; ?>/manage">Ready For Delivery
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/delivered/<?php echo $order['order_id']; ?>/manage">Delivered
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <?php } elseif ($order['order_status'] == 'rejected') { ?>
                                                        <td class="chip lighten-4 red">Rejected</td>
                                                        <td>
                                                            <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                            <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/approved/<?php echo $order['order_id']; ?>/manage" type="submit" name="action">Approved
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/cancelled/<?php echo $order['order_id']; ?>/manage">Cancelled
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/readyfordelivery/<?php echo $order['order_id']; ?>/manage">Ready For Delivery
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/delivered/<?php echo $order['order_id']; ?>/manage">Delivered
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <?php } elseif ($order['order_status'] == 'cancelled') { ?>
                                                        <td class="chip grey lighten-3">Cancelled</td>
                                                        <td>
                                                            <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                            <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/approved/<?php echo $order['order_id']; ?>/manage" type="submit" name="action">Approved
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/rejected/<?php echo $order['order_id']; ?>/manage" name="action">Rejected
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/readyfordelivery/<?php echo $order['order_id']; ?>/manage">Ready For Delivery
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/delivered/<?php echo $order['order_id']; ?>/manage">Delivered
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <?php } elseif ($order['order_status'] == 'readyfordelivery') { ?>
                                                        <td class="chip lighten-4 orange">Ready For Delivery</td>
                                                        <td>
                                                            <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                            <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/approved/<?php echo $order['order_id']; ?>/manage" type="submit" name="action">Approved
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/rejected/<?php echo $order['order_id']; ?>/manage" name="action">Rejected
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/cancelled/<?php echo $order['order_id']; ?>/manage">Cancelled
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/delivered/<?php echo $order['order_id']; ?>/manage">Delivered
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <?php } else { ?>
                                                        <td class="chip green lighten-5">Delivered</td>
                                                        <td>
                                                            <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                            <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/approved/<?php echo $order['order_id']; ?>/manage" type="submit" name="action">Approved
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/rejected/<?php echo $order['order_id']; ?>/manage" name="action">Rejected
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/cancelled/<?php echo $order['order_id']; ?>/manage">Cancelled
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a style="padding: 10px;" href="<?php echo base_url(); ?>admin/readyfordelivery/<?php echo $order['order_id']; ?>/manage">Ready For Delivery
                                                                    </a>
                                                                </li>

                                                            </ul>
                                                        </td>
                                                    <?php } ?>



                                                    <td></td>
                                               
                                                </tr>
                                            <?php $s_no++;
                                            endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>