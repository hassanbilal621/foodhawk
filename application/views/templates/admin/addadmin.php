<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper">
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Add Admin</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <div class="users-list-table">
               <div class="card">
                  <div class="card-content">
                     <?php echo form_open_multipart('admin/addadmin'); ?>
                     <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                           <input type="text" name="name" required>
                           <label for="">First & Last Name</label>
                        </div>

                        <div class="input-field col s12 m12 l6">
                           <input type="number" class="validate" id="phone-code" name="mobile" required>
                           <label for="">Mobile No</label>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                           <input type="email" class="validate"  name="email" required>
                           <label for="">E Mail</label>
                        </div>

                        <div class="input-field col s12  m12 l6">
                           <input type="password" name="password" required>
                           <label for="">Password</label>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12 m12 l12">
                           <input type="text" name="address" required>
                           <label for="">Address</label>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select Country</label>
                           <select name="country" class="select2  browser-default">
                              <?php foreach ($countries as $country) : ?>
                                 <option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                              <?php endforeach; ?>

                           </select>
                        </div>

                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select City</label>
                           <select name="city" class="select2  browser-default">
                              <?php foreach ($cities as $city) : ?>
                                 <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                              <?php endforeach; ?>
                           </select>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select Zip Code</label>
                           <select name="zipcode" class="select2  browser-default">
                           <?php foreach ($zipcodes as $zipcode) : ?>
                                 <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                              <?php endforeach; ?>
                           </select>
                        </div>
                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select Role</label>
                           <select name="role" class="select2  browser-default">
                              <option value="1">Super admin</option>
                              <option value="2">Admin</option>

                           </select>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12 m12 l12">
                           <div id="view-file-input">

                              <div class="file-field input-field">
                                 <div class="btn">
                                    <span>Upload Your Corporate Application</span>
                                    <input type="file" name="userfile" accept="image/png,image/jpg,image/jpeg,application/pdf" required>
                                 </div>
                                 <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                 </div>
                                 <span>Upload only PDF and JPG Images</span>
                              </div>
                           </div>
                           <div class="row">
                              <div class=" col s12">
                                 <button class="btn waves-effect waves-light border-round submit right">Register Now</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close(); ?>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>