<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Add User</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col s12">
      <div class="container">
         <div class="users-list-table">
            <div class="card">
               <div class="card-content">
                  <?php echo form_open_multipart('admin/adduser'); ?>
                  <div class="row">
                     <div class="input-field col s12">
                     </div>
                  </div>
                  <div class="row">
                     <div class="col s12">
                        <div class="row margin">
                           <div class="input-field col s12  m12 l6">
                              <input type="text" name="name" placeholder="Type Your First  & Last Name" required>
                           </div>
                           <div class="input-field col s12 m12 l6">
                              <input type="number" class="validate" name="mobile" placeholder="Type Your Mobile No" required>
                           </div>
                        </div>
                        <div class="row margin">
                           <div class="input-field col s12  m12 l6">
                              <input type="email" name="email" class="validate" placeholder="Type Your email" required>
                           </div>

                           <div class="input-field col s12  m12 l6">
                              <input type="password" name="password" placeholder="Type Your Password" required>
                           </div>
                           <div class="row margin">
                              <div class="input-field col s12 m12 l6">
                                 <input type="text" name="address" placeholder="Type Your address" required>
                              </div>

                              <div class="input-field col s12 m12 l6">
                                 <label for="" class="active">Select Country</label>
                                 <select name="country" class="select2  browser-default">
                                    <?php foreach ($countries as $country) : ?>
                                       <option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                                    <?php endforeach; ?>

                                 </select>
                              </div>

                           </div>
                           <div class="row margin">
                              <div class="input-field col s12 m12 l6">
                                 <label for="" class="active">Select City</label>
                                 <select name="city" class="select2  browser-default">
                                    <?php foreach ($cities as $city) : ?>
                                       <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>

                              <div class="input-field col s12 m12 l6">
                                 <label for="" class="active">Select Zip Code</label>
                                 <select name="zipcode" class="select2  browser-default">
                                    <?php foreach ($zipcodes as $zipcode) : ?>
                                       <option value="<?php echo $zipcode['zipcode']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           <div class="row margin">
                              <div class="input-field col s12 m12 l6">
                                 <input id="state" name="state" type="text" class="validate" data-error=".errorTxt3">
                                 <label for="state">State</label>
                              </div>
                              <div class="input-field col s12 m12 l6">
                                 <input type="text" id="credit-demo" name="credit" placeholder="Type Your credit Card No" required>
                                 <label for="">credit Card</label>
                              </div>   
                           </div>
                           <div class="row margin">
                              <div class="input-field col s12 m12 l12">
                                 <div id="view-file-input">

                                    <div class="file-field input-field">
                                       <div class="btn">
                                          <span>Upload Your Corporate Application</span>
                                          <input type="file" name="userfile"  accept="image/png,image/jpg,image/jpeg,application/pdf" required>
                                       </div>
                                       <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text">
                                       </div>
                                       <span>Upload only PDF and JPG Images</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <button class="btn waves-effect waves-light border-round submit right">Register Now</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close(); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>