<!-- BEGIN: SideNav-->

<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
        <div class="brand-sidebar" style="background-color: #a01515">
            <h1 class="logo-wrapper"><a class=" darken-1" href="<?php echo base_url();?>admin/index"><img class="hide-on-med-and-down" src="<?php echo base_url(); ?>assets/app-assets/images/logo/logo.png" style="width: 200px !important;margin: 12px 0 0px 5px;"alt=" FoodHawk"></span></a><a class="navbar-toggler" href="#"><i class="material-icons" style="color: white;">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">

        <!---/////////////////////////////////////////-------------Dashboard-----------///////////////////////////////////////// -->

        <li class="navigation-header">
            <a class="navigation-header-text black-text">Dashboard</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/"><i class="material-icons">computer</i><span class="menu-title" data-i18n="">Dashboard</span></a>
        </li>

        <!---/////////////////////////////////////////-------------admin Management-----------///////////////////////////////////////// -->

        <li class="navigation-header">
            <a class="navigation-header-text black-text"> admin Management</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/addadmin" data-i18n=""><i class="material-icons">person_add</i><span style="color: #444444;">Add Admin</span></a></li>
        <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/manageadmins" data-i18n=""><i class="material-icons">person</i><span style="color: #444444;">Manage Admin</span></a></li>
   



    <!---/////////////////////////////////////////-------------users Management-----------///////////////////////////////////////// -->

    <li class="navigation-header">
        <a class="navigation-header-text black-text"> Users Management</a>
        <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/adduser" data-i18n=""><i class="material-icons">person_add</i><span style="color: #444444;">Add User</span></a></li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/manageusers" data-i18n=""><i class="material-icons">person</i><span style="color: #444444;">Manage User</span></a></li>
   
    <!---/////////////////////////////////////////------------- Order-----------///////////////////////////////////////// -->

    <li class="navigation-header">
        <a class="navigation-header-text black-text">Order</a>
        <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/manageorder" data-i18n=""><i class="material-icons">event_note</i><span style="color: #444444;">Manage Order</span></a></li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/Pendingapproval" data-i18n=""><i class="material-icons">history</i><span style="color: #444444;">Pending Approval</span></a></li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/readyfordelivered" data-i18n=""><i class="material-icons">event_available</i><span style="color: #444444;">Ready For Delivery</span></a></li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/delivery" data-i18n=""><i class="material-icons">done</i><span style="color: #444444;">Delivery</span></a></li>
 

    <!---/////////////////////////////////////////-------------setting Management-----------///////////////////////////////////////// -->

    <li class="navigation-header">
        <a class="navigation-header-text black-text">Setting Management</a>
        <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/setting" data-i18n=""><i class="material-icons">settings_applications</i><span style="color: #444444;">Site Setting</span></a></li>
 
     <!---/////////////////////////////////////////-------------setting Management-----------///////////////////////////////////////// -->

     <li class="navigation-header">
        <a class="navigation-header-text black-text">Reports</a>
        <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/report" data-i18n=""><i class="material-icons">insert_drive_file</i><span style="color: #444444;">Orders Reports</span></a></li>
 

    </ul>

    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->