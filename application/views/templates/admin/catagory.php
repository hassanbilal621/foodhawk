<div id="main">

   <div class="col s12">
      <div class="section" id="materialize-sparkline">
         <div class="section">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <h4 class="card-title">Add Categories</h4>
                     <?php echo form_open('admin/category'); ?>
                     <div class="input-field col s6">
                        <input type="text" name="category_name" placeholder="Add Catagory" required>
                     </div>
                     <div class="input-field col s6">
                        <input type="text" name="category_price" placeholder="Add Catagory Price" required>
                     </div>
                     <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                     </button>
                  </div>
                  <?php echo form_close(); ?>

                  <h4 class="card-title">Manage Expence catagory</h4>
                  <div class="row">
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>Catagory id</th>
                              <th>Catagory Name</th>
                              <th>Catagory Price</th>
                              <th>edit</th>
                              <th>delete</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $s_no = '1';
                           foreach ($categories as $category) : ?>
                              <tr>

                                 <td><?php echo $s_no ?></td>
                                 <td><?php echo $category['category_name']; ?></td>
                                 <td><?php echo $category['category_price']; ?></td>
                                 <td>
                                    <a class="waves-effect waves-light mr-1 mb-1 modal-trigger" onclick="loadcatagoryinfo(this.id)" id="<?php echo $category['category_id']; ?>" type="submit" href="#modal3" name="action">
                                       <i class="material-icons left">edit</i>
                                    </a>
                                 </td>
                                 <td>
                                    <a class="waves-effect waves-light  mr-1 mb-1" href="<?php echo base_url(); ?>admin/deletecategory/<?php echo $category['category_id']; ?>" type="submit" name="action">
                                       <i class="material-icons left">delete</i>
                                    </a>
                                 </td>
                              </tr>
                           <?php $s_no++;
                           endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

</div>
</div>
</div>
</div>
</div>
<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadcatagoryinfo(catagoryid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_catagory/" + catagoryid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>