<div id="main">
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
            <div class="row">
                <div class="col s12 m6 l6">
                    <h5 class="breadcrumbs-title mt-0 mb-0">
                        <span<span style="font-weight: bold;">Edit User</span>
                    </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section users-edit">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12">
                                    <!-- users edit media object start -->
                                    <div class="media display-flex align-items-center mb-2">
                                        <a class="mr-2" href="#">
                                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['profile_image']; ?>" alt="users avatar" class="z-depth-4 circle" height="64" width="64">
                                        </a>
                                        <div class="media-body">
                                            <h5 class="capitalize media-heading mt-0"><?php echo $profile['name']; ?></h5>
                                            <div class="user-edit-btns display-flex">
                                                <?php echo form_open_multipart('admin/profileimage'); ?>
                                                <div class="row">
                                                    <input type="file" name="userfile">
                                                    <input type="hidden" name="user_id" value="<?php echo $profile['user_id']; ?>">
                                                    <button type="submit" class="btn submit">Change</button>
                                                </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="tabs mb-2 row">
                                        <li class="tab">
                                            <a class="display-flex align-items-center" id="information-tab" href="#information">
                                                <i class="material-icons mr-2">error_outline</i><span>Information</span>
                                            </a>
                                        </li>
                                        <li class="tab">
                                            <a class="display-flex align-items-center" id="address-tab" href="#address">
                                                <i class="material-icons mr-2">place</i><span>Address</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php echo form_open('admin/updateprofile') ?>
                                    <div id="information">
                                        <div class="row">
                                            <div class="col s12 m6 l6">
                                                <div class="row">
                                                    <div class="col s12 input-field">
                                                        <input id="username" name="name" type="text" class="validate" value="<?php echo $profile['name']; ?>" data-error=".errorTxt1">
                                                        <input type="hidden" name="user_id" value="<?php echo $profile['user_id']; ?>">
                                                        <label for="username">Username</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="credit_card" name="credit_card" type="number" class="validate" value="<?php echo $profile['credit_card']; ?>" data-error=".errorTxt3">
                                                        <label for="credit_card">Credit Card</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="mobile" name="mobile" type="number" class="validate" value="<?php echo $profile['mobile']; ?>" data-error=".errorTxt2">
                                                        <label for="mobile">Mobile</label>
                                                        <small class="errorTxt2"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="name_of_organization" name="name_of_organization" type="text" class="validate" value="<?php echo $profile['name_of_organization']; ?>" data-error=".errorTxt1">
                                                        <label for="name_of_organization">name_of_organization</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="diet_prefrence" name="diet_prefrence" type="text" class="validate" value="<?php echo $profile['diet_prefrence']; ?>" data-error=".errorTxt2">
                                                        <label for="diet_prefrence">diet_prefrence</label>
                                                        <small class="errorTxt2"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="allergies" name="allergies" type="text" class="validate" value="<?php echo $profile['allergies']; ?>" data-error=".errorTxt3">
                                                        <label for="allergies">allergies</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12 m6 l6">
                                                <div class="row">
                                                    <div class="col s12 input-field">
                                                        <input id="email" name="email" type="email" class="validate" value="<?php echo $profile['email']; ?>" data-error=".errorTxt2" readonly>
                                                        <label for="email">E_mail</label>
                                                        <small class="errorTxt2"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="cantact_name" name="cantact_name" type="text" class="validate" value="<?php echo $profile['cantact_name']; ?>" data-error=".errorTxt1">
                                                        <label for="cantact_name">cantact_name</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="phone_num" name="phone_num" type="number" class="validate" value="<?php echo $profile['phone_num']; ?>" data-error=".errorTxt3">
                                                        <label for="phone_num">Phone No</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="account_note" name="account_note" type="text" class="validate" value="<?php echo $profile['account_note']; ?>" data-error=".errorTxt3">
                                                        <label for="account_note">account_note</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="budget_prefrence" name="budget_prefrence" type="number" class="validate" value="<?php echo $profile['budget_prefrence']; ?>" data-error=".errorTxt3">
                                                        <label for="budget_prefrence">budget_prefrence</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input id="day_Schedule" name="day_Schedule" type="text" class="validate" value="<?php echo $profile['day_Schedule']; ?>" data-error=".errorTxt3">
                                                        <label for="day_Schedule">day_Schedule</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="address">
                                        <div class="row">
                                            <div class="col s12 m6 l6">
                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select Country</label>
                                                    <select name="country" class="select2  browser-default">
                                                        <?php foreach ($countries as $country) : ?>
                                                            <?php if ($profile['country'] == $country['country_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select City</label>
                                                    <select name="city" class="select2  browser-default">
                                                        <?php foreach ($cities as $city) : ?>

                                                            <?php if ($profile['city'] == $city['city_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php } ?>

                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col s12 m6 l6">
                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select Zip Code</label>
                                                    <select name="zipcode" class="select2  browser-default">
                                                        <?php foreach ($zipcodes as $zipcode) : ?>
                                                            <?php if ($profile['zipcode'] == $zipcode['zipcode_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <input id="state" name="state" type="text" class="validate" value="<?php echo $profile['state']; ?>" data-error=".errorTxt3">
                                                    <label for="state">State</label>
                                                    <small class="errorTxt3"></small>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="col s12 input-field">
                                                        <input id="delivery_instruction" name="delivery_instruction" type="text" class="validate" value="<?php echo $profile['delivery_instruction']; ?>" data-error=".errorTxt1">
                                                        <label for="delivery_instruction">delivery_instruction</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <button type="submit" class="btn submit mr-2 ">Save changes</button>
                                        <a href="<?php echo base_url(); ?>admin/manageusers" class="btn delete btn-light">Cancel</a>
                                    </div>
                                    <?php form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>