<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark darken-4 gradient-shadow navbar-border navbar-shadow" style="background-color: #ed4242">
      <div class="footer-copyright">
        <div class="container"><span>&copy; 2020 All rights reserved.</span></div>
      </div>
    </footer>

    <!-- END: Footer-->



      <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/search.js"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js"></script>
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/customizer.js"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-media.js"></script>
    <!-- END PAGE LEVEL JS-->
   </body>
</html> 