<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Manage Admin</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <!-- users list start -->
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- datatable start -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>#admin id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>registration Date</th>
                                    <th>Status</th>
                                    <!-- <th>Edit</th> -->
                                    <th>View</th>
                                    <th>Delete</th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php $s_no = '1';
                                 foreach ($admins as $admin) : 
                                 
                                 if($admin['admin_id'] != $this->session->admindata('food_admin_id') ){
                                 ?>
                                    <tr>
                                       <td></td>
                                       <td><?php echo $admin['admin_id']; ?></td>
                                       <td><?php echo $admin['name']; ?></td>
                                       <td><?php echo $admin['email']; ?></td>
                                       <td><?php echo $admin['mobile']; ?></td>
                                       <td><?php echo $admin['registration_date']; ?></td>
                                       <?php if ($admin['status'] == 'active') { ?>
                                          <td class="switch">
                                             <label>
                                                Deactivate <input type="checkbox" checked onclick="disableadmin(this.id)" id="<?php echo $admin['admin_id']; ?>">
                                                <span class="lever"></span>
                                                Activate </label>
                                          </td>
                                       <?php } else { ?><td class="switch">
                                             <label>
                                             Deactivate <input type="checkbox" onclick="enableadmin(this.id)" id="<?php echo $admin['admin_id']; ?>">
                                                <span class="lever"></span>
                                                Activate </label>
                                          </td>
                                       <?php  } ?>
                                       </td>

                                       <!-- <td><a href="page-users-edit.html"><i class="material-icons">edit</i></a></td> -->
                                       <td><a href="<?php echo base_url(); ?>admin/editadmin/<?php echo $admin['admin_id']; ?>"><i class="material-icons">edit</i></a></td>
                                       <td><a onclick="del(this.id)" id="<?php echo $admin['admin_id']; ?>"><i class="material-icons">delete</i></a></td>
                                       <td></td>
                                    </tr>
                                 <?php $s_no++;

                                 }
                                 endforeach; ?>

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>

<script>
    function enableadmin(adminid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_admin_enable/" + adminid,
            success: function(data) {
                location.reload();


            }
        });
    }
</script>
<script>
    function disableadmin(adminid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_admin_disable/" + adminid,
            success: function(data) {
                location.reload();




            }
        });
    }
</script>

<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No',
            delete: 'Yes'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deleteadmin/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>