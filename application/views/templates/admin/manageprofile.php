<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Manage Users</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <!-- users list start -->
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- datatable start -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>#user id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>registration Date</th>
                                    <th>Status</th>
                                    <!-- <th>Edit</th> -->
                                    <th>edit</th>
                                    <th>Delete</th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                              <?php $s_no = '1';	
                               foreach ($profiles as $profile) : ?>
                                    <tr>
                                       <td></td>
                                       <td>#<?php echo $profile['user_id']; ?></td>
                                       <td><?php echo $profile['name']; ?></td>
                                       <td><?php echo $profile['email']; ?></td>
                                       <td><?php echo $profile['mobile']; ?></td>
                                       <td><?php echo $profile['registration_date']; ?></td>
                                       <?php if ($profile['status'] == 'active') { ?>
                                          <td class="switch">
                                             <label>
                                                Deactivate <input type="checkbox" checked onclick="disableuser(this.id)" id="<?php echo $profile['user_id']; ?>">
                                                <span class="lever"></span>
                                                Activate </label>
                                          </td>
                                       <?php } else { ?><td class="switch">
                                             <label>
                                             Deactivate <input type="checkbox" onclick="enableuser(this.id)" id="<?php echo $profile['user_id']; ?>">
                                                <span class="lever"></span>
                                                Activate </label>
                                          </td>
                                       <?php  } ?>
                                       </td>

                                       <!-- <td><a href="page-users-edit.html"><i class="material-icons">edit</i></a></td> -->
                                       <td><a href="<?php echo base_url(); ?>admin/editprofile/<?php echo $profile['user_id']; ?>"><i class="material-icons">edit</i></a></td>
                                       <td><a onclick="del(this.id)" id="<?php echo $profile['user_id']; ?>"><i class="material-icons">delete</i></a></td>
                                       <td></td>
                                    </tr>
                                    <?php $s_no++;
                                 endforeach; ?>

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>


<script>
    function enableuser(userid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_user_enable/" + userid,
            success: function(data) {
                location.reload();


            }
        });
    }
</script>
<script>
    function disableuser(userid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_user_disable/" + userid,
            success: function(data) {
                location.reload();




            }
        });
    }
</script>


<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No, Please!',
            delete: 'Yes, Delete It'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deleteprofile/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>