<div class="row login-bg">
    <div class="col s12">
        <div class="container">
            <div id="login-page" class="row">
                <div class="col s8 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                    <?php echo form_open('admin/newpassword'); ?>
                    <div class="login-form">
                        <div class="row">
                            <div class="input-field col s12 center">
                                <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/TEXT COLOR(2).png" alt="" style="width: 75%;margin: 10px 0px 10px 0;" class="responsive-img valign">
                                <h5 class="ml-4">New Password</h5>
                                <h6 class="center login-form-text">Type Your New Password</h6>
                            </div>
                        </div>

                        <div class="row margin">
                            <div class="input-field col s12">
                                <i style="color:#ed4242;" class="material-icons prefix pt-2">lock</i>
                                <input id="username" name="password" type="password" placeholder="Type Your password" required>
                                <input name="email" type="hidden" value="<?php echo $admindetail['email'];?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button type="submit" name="login" class="btn waves-effect submit border-round waves-light col s12">Save Password</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <p class=" medium-small"><a style="font-size: large;" href="<?php echo base_url(); ?>admin/login">Login</a></p>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>