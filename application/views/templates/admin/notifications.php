<div id="main">
    <div class="row">
    <div id="breadcrumbs-wrapper">
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Notifications</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <div class="card">
                    <div class="card-content">
                        <!-- datatable start -->
                        <div class="row">
                            <div class="col s12">
                                <table id="page-length-option" class="display">
                                    <thead>
                                        <tr>
                                            <th>#S-N0</th>
                                            <th>Subject</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>Mark As Read</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $s_no = '1';
                                        foreach ($notifications as $notification) : ?>
                                            <tr>
                                                <td><?php echo $s_no ?></td>
                                                <td><?php echo $notification['notification_title']; ?></td>
                                                <td><?php echo $notification['notification']; ?></td>
                                                <td><?php echo $notification['date_time']; ?></td>
                                                <td id="<?php echo $notification['notification_id']; ?>"><?php echo $notification['status']; ?></td>
                                                <?php if ($notification['status'] == 'read') { ?>
                                                    <td> <label>
                                                            <input onclick="unreadnotification(this.id)" id="<?php echo $notification['notification_id']; ?>" type="checkbox" checked="checked" />
                                                            <span> </span>
                                                        </label> <?php } else { ?></td>
                                                    <td>
                                                        <label>
                                                            <input onclick="readnotification(this.id)" id="<?php echo $notification['notification_id']; ?>" type="checkbox" />
                                                            <span> </span>
                                                        </label>
                                                    </td>
                                                <?php } ?>
                                                <td></td>
                                            </tr>
                                        <?php $s_no++;
                                        endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
            </div>
        </div>
    </div>
</div>

<script>
    function readnotification(notificationid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_notification_read/" + notificationid,
            success: function(data) {
                location.reload();


            }
        });
    }
</script>
<script>
    function unreadnotification(notificationid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_notification_unread/" + notificationid,
            success: function(data) {
                location.reload();




            }
        });
    }
</script>