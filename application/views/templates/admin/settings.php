<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Setting Management</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="row">
            <div class="col s12 m12 l12">
               <div id="basic-tabs" class="card card card-default scrollspy">
                  <div class="card-content">

                     <div class="row">
                        <div class="col s12">
                           <div class="row" id="main-view">
                              <div class="col s12">
                                 <ul class="tabs tab-demo z-depth-1">
                                    <li class="tab col m2"><a class="active" href="#guest_setting">Guest Setting</a></li>
                                    <li class="tab col m2"><a href="#categories">Manage Categories</a></li>
                                    <li class="tab col m2"><a href="#dietmanagement">Diet Management</a></li>
                                    <!-- <li class="tab col m2"><a href="#zipcode">Zip Code</a></li>
                                    <li class="tab col m2"><a href="#countrysetup">Country Setup</a></li> -->
                                 </ul>
                              </div>
                              <div class="col s12">
                                 <div id="guest_setting" class="col s12">
                                    <div class="row">
                                       <div id="breadcrumbs-wrapper">
                                          <!-- Search for small screen-->
                                          <div class="container">
                                             <div class="row">
                                                <div class="col s12 m6 l6">
                                                   <h5 class="breadcrumbs-title mt-0 mb-0">
                                                      <span<span style="font-weight: bold;">Guest</span>
                                                   </h5>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="section">
                                             <div class="card">
                                                <div class="card-content">
                                                   <?php echo form_open('admin/updateguest'); ?>
                                                   <h4 class="card-title">set minimum range of guests for food order form</h4>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <label for="" class="active">Number OF Guest</label>
                                                         <input type="number" class="validate" pattern="[0-9]*"name="guest" placeholder="Update Your guest" value="<?php echo $guests['guest']; ?>" required>
                                                         <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Update guest
                                                            <i class="material-icons right">save</i>
                                                         </button>
                                                      </div>
                                                   </div>
                                                   <?php echo form_close(); ?>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="categories" class="col s12">
                                    <div class="row">
                                       <div id="breadcrumbs-wrapper">
                                          <!-- Search for small screen-->
                                          <div class="container">
                                             <div class="row">
                                                <div class="col s12 m6 l6">
                                                   <h5 class="breadcrumbs-title mt-0 mb-0">
                                                      <span<span style="font-weight: bold;">Manage Categories</span>
                                                   </h5>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col s12">
                                          <div class="section" id="materialize-sparkline">
                                             <div class="section">
                                                <div class="card">
                                                   <div class="card-content">
                                                      <div class="row">
                                                         <?php echo form_open('admin/category'); ?>
                                                         <div class="input-field col s6">
                                                            <input type="text" name="category_name" required>
                                                         <label for="">Category Price</label>
                                                         </div>
                                                         <div class="input-field col s6">
                                                            <input type="number" name="category_price" required>
                                                         <label for="">Category Price</label>
                                                         </div>
                                                         <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                                                            <i class="material-icons right">send</i>
                                                         </button>
                                                         <?php echo form_close(); ?>
                                                      </div>

                                                      <h4 class="card-title">Manage Expence category</h4>
                                                      <div class="row">
                                                         <table id="page-length-option" class="display" style="width: 100%!important;">
                                                            <thead>
                                                               <tr>
                                                                  <th>category id</th>
                                                                  <th>category Name</th>
                                                                  <th>category Price</th>
                                                                  <th>edit</th>
                                                                  <th>delete</th>
                                                               </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php $s_no = '1';
                                                               foreach ($categories as $category) : ?>
                                                                  <tr>

                                                                     <td><?php echo $s_no ?></td>
                                                                     <td><?php echo $category['category_name']; ?></td>
                                                                     <td><?php echo $category['category_price']; ?></td>
                                                                     <td>
                                                                        <a class="waves-effect waves-light mr-1 mb-1 modal-trigger" onclick="loadcategoryinfo(this.id)" id="<?php echo $category['category_id']; ?>" type="submit" href="#modal3" name="action">
                                                                           <i class="material-icons left">edit</i>
                                                                        </a>
                                                                     </td>
                                                                     <td>
                                                                        <a class="waves-effect waves-light  mr-1 mb-1" onclick="delcat(this.id)" id="<?php echo $category['category_id']; ?>" type="submit" name="action">
                                                                           <i class="material-icons left">delete</i>
                                                                        </a>
                                                                     </td>
                                                                  </tr>
                                                               <?php $s_no++;
                                                               endforeach; ?>
                                                               </tfoot>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                    </div>
                                 </div>
                                 <div id="dietmanagement" class="col s12">
                                    <div class="row">
                                       <div id="breadcrumbs-wrapper">
                                          <!-- Search for small screen-->
                                          <div class="container">
                                             <div class="row">
                                                <div class="col s12 m6 l6">
                                                   <h5 class="breadcrumbs-title mt-0 mb-0">
                                                      <span<span style="font-weight: bold;">Diet Management</span>
                                                   </h5>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col s12">
                                          <div class="section">
                                             <div class="card">
                                                <div class="card-content">
                                                   <div class="row">
                                                      <?php echo form_open('admin/diet'); ?>
                                                      <div class="input-field col s6">
                                                         <input type="text" name="diet_name" placeholder="Add diet" required>
                                                         <label for="">Diet</label>
                                                      </div>
                                                      <div class="input-field col s6">
                                                         <input type="number" name="diet_price" required>
                                                         <label for="">Diet Price</label>
                                                      </div>
                                                      <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                                                         <i class="material-icons right">send</i>
                                                      </button>
                                                   </div>
                                                   <?php echo form_close(); ?>

                                                   <h4 class="card-title">Manage Expence diet</h4>
                                                   <div class="row">
                                                      <table id="data-table-simple" class="display" style="width: 100%!important;">

                                                         <thead>
                                                            <tr>
                                                               <th>diet id</th>
                                                               <th>diet Name</th>
                                                               <th>diet Price</th>
                                                               <th>edit</th>
                                                               <th>delete</th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php $s_no = '1';
                                                            foreach ($diets as $diet) : ?>
                                                               <tr>
                                                                  <td><?php echo $s_no ?></td>
                                                                  <td><?php echo $diet['diet_name']; ?></td>
                                                                  <td><?php echo $diet['diet_price']; ?></td>
                                                                  <td>
                                                                     <a class="waves-effect waves-light mr-1 mb-1 modal-trigger" onclick="loaddietinfo(this.id)" id="<?php echo $diet['diet_id']; ?>" type="submit" href="#modal3" name="action">
                                                                        <i class="material-icons left">edit</i>
                                                                     </a>
                                                                  </td>
                                                                  <td>
                                                                     <a class="waves-effect waves-light  mr-1 mb-1"  onclick="deldiet(this.id)" id="<?php echo $diet['diet_id']; ?>" type="submit" name="action">
                                                                        <i class="material-icons left">delete</i>
                                                                     </a>
                                                                  </td>
                                                               </tr>
                                                            <?php $s_no++;
                                                            endforeach; ?>
                                                            </tfoot>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- <div id="zipcode" class="col s12">
                                    <div class="row">
                                       <div id="breadcrumbs-wrapper">
                                          <div class="container">
                                             <div class="row">
                                                <div class="col s12 m6 l6">
                                                   <h5 class="breadcrumbs-title mt-0 mb-0">
                                                      <span<span style="font-weight: bold;">Manage Zip Code</span>
                                                   </h5>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col s12">
                                          <div class="section" id="materialize-sparkline">
                                             <div class="section">
                                                <div class="card">
                                                   <div class="card-content">
                                                      <div class="row">
                                                         <?php echo form_open('admin/zipcode'); ?>
                                                         <h4 class="card-title">Add Zipode</h4>
                                                         <div class="row">
                                                            <div class="input-field col s12">
                                                               <input type="number" name="zipcode" placeholder="Type Zipode" required>
                                                            </div>
                                                            <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save Zipode
                                                               <i class="material-icons right">send</i>
                                                            </button>
                                                         </div>
                                                         <?php echo form_close(); ?>
                                                      </div>
                                                      <h4 class="card-title">Manage Zip Code</h4>
                                                      <div class="row">
                                                         <table id="scroll-dynamic" class="display" style="width: 100%!important;">>

                                                            <thead>
                                                               <tr>
                                                                  <th>#</th>
                                                                  <th>Zipcodes</th>
                                                                  <th>Edit</th>
                                                                  <th>Delete</th>
                                                               </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php $s_no = '1';
                                                               foreach ($zipcodes as $zipcode) : ?>
                                                                  <tr>
                                                                     <td><?php echo $s_no ?></td>
                                                                     <td><?php echo $zipcode['zipcode']; ?></td>
                                                                     <td><a href="page-users-edit.html"><i class="material-icons">edit</i></a></td>
                                                                     <td><a href="page-users-view.html"><i class="material-icons">delete</i></a></td>
                                                                  </tr>
                                                               <?php $s_no++;
                                                               endforeach; ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div id="countrysetup" class="col s12">
                                    <div id="breadcrumbs-wrapper">
                                       <div class="container">
                                          <div class="row">
                                             <div class="col s12 m6 l6">
                                                <h5 class="breadcrumbs-title mt-0 mb-0">
                                                   <span<span style="font-weight: bold;">Manage Zip Code</span>
                                                </h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col s12">
                                       <div class="section" id="materialize-sparkline">
                                          <div class="section">
                                             <div class="card">
                                                <div class="card-content">
                                                   <div class="row">
                                                      <?php echo form_open('admin/#'); ?>

                                                      <div class="input-field col s6">
                                                         <select name="" id="" required>
                                                            <?php foreach ($countries as $country) : ?>
                                                               <option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php endforeach; ?>
                                                         </select>
                                                         <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Active Country
                                                            <i class="material-icons right">save</i>
                                                         </button>
                                                      </div>
                                                      <?php echo form_close(); ?>
                                                      <?php echo form_open('admin/#'); ?>

                                                      <div class="input-field col s6">
                                                         <select name="city_state_id" id="" required>
                                                            <?php foreach ($cities as $city) : ?>
                                                               <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php endforeach; ?>
                                                         </select>
                                                         <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Active City
                                                            <i class="material-icons right">save</i>
                                                         </button>
                                                      </div>
                                                      <?php echo form_close(); ?>

                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div> -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadcategoryinfo(categoryid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_category/" + categoryid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loaddietinfo(dietid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_diet/" + dietid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>


<script>
   function delcat(id) {

      swal({
         title: "Are you sure?",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No',
            delete: 'Yes'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deletecategory/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>

<script>
   function deldiet(id) {

      swal({
         title: "Are you sure?",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No',
            delete: 'Yes'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deletediet/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>
<script>
   (function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));


// Install input filters.
$("#intTextBox").inputFilter(function(value) {
  return /^-?\d*$/.test(value); });
</script>