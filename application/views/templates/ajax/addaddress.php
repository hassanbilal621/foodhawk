<?php echo form_open('users/addaddress') ?>
<div class="row">
<a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
    <div class="input-field col s8">
        <textarea name="address" class="discrip" placeholder="Type Your New Address" required></textarea>
        <input type="hidden" name="addressid" value="<?php echo $profile['user_id']; ?>"> 
    </div>
    <div class="input-field col s4">
        <select class="select2 browser-default" name="zipcode" required>
            <option disabled selected Value>Select Zip Code</option>
            <option value="74400">74400</option>
            <option value="74401">74401</option>
            <option value="74402">74402</option>
            <option value="74403">74403</option>
            <option value="74404">74404</option>
            <option value="74405">74405</option>
            <option value="74406">74406</option>
        </select>
    </div>
</div>

<button type="submit" class="waves-effect waves-light  btn submit box-shadow-none mb-2 border-round right">Save</button>


<?php echo form_close(); ?>