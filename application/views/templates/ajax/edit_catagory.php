<div class="row">
    <div class="col s12">
        <h4 class="card-title">update Categories</h4>
        <a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
    </div>
    <?php echo form_open('admin/updatecategory'); ?>
    <div class="row">

        <div class="input-field col s6">
            <input type="text" name="category_name" placeholder="Add Catagory" value="<?php echo $category['category_name']; ?>" required>
            <input type="hidden" name="category_id" value="<?php echo $category['category_id']; ?>">
        </div>
        <div class="input-field col s6">
            <input type="text" name="category_price" placeholder="Add Catagory Price" value="<?php echo $category['category_price']; ?>" required>
        </div>
        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    </div>
    <?php echo form_close(); ?>
</div>