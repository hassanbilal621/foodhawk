	<div class="row">
		<?php echo form_open('admin/city'); ?>
		<h4 class="card-title">Add City</h4>
		<div class="row">
			<div class="input-field col s6">
				<select class="browser-default" name="country_city_id" required>
					<?php foreach ($countries as $country) : ?>
						<option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="input-field col s6">
				<input type="text" name="city_name" placeholder="Select Country First Then Type city" required>
			</div>
			<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">save City
				<i class="material-icons right">send</i>
			</button>
		</div>
		<?php echo form_close(); ?>
	</div>