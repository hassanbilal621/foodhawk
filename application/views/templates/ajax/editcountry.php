	<div class="row">
		<?php echo form_open('admin/country'); ?>
		<h4 class="card-title">Update Country</h4>
		<div class="row">
			<div class="input-field col s12">
				<input type="text" name="country_name" placeholder="Type Country" required>
				<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save Country
					<i class="material-icons right">send</i>
				</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>