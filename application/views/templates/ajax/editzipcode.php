	<div class="row">
<a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
		<?php echo form_open('admin/zipcode'); ?>
		<h4 class="card-title">Add Zipode</h4>
		<div class="row">
			<div class="input-field col s12">
				<input type="number" name="zipcode" placeholder="Type Zipode" required>
				<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save Zipode
					<i class="material-icons right">send</i>
				</button>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>