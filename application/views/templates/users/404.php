<div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section section-404 p-0 m-0 height-100vh">
                    <div class="row">
                        <!-- 404 -->
                        <div class="col s12 center-align white">
                            <img src="<?php echo base_url();?>assets/app-assets/images\background/404.jpg" class="bg-image-404" alt="">
                            <h6 class="mb-2">Page Not Found</h6>
                            <a class="btn waves-effect waves-light submit gradient-shadow mb-4" href="<?php echo base_url();?>users/index">Back
                                TO Home</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-overlay"></div>
        </div>