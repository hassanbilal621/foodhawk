<div id="main">
   <div class="row"><div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Add Order</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
         <div class="col s12">
            <div id="input-fields" class="card card-tabs">
               <div class="card-content">
                  <div class="card-title">
                     <div class="row">
                        <div class="col s12 m6 l10">
                           <h4 class="card-title">Food Hawk Form</h4>
                        </div>
                     </div>
                  </div>
                  <div id="view-input-fields">
                     <div class="row">
                        <div class="col s12">
                           <form class="row">
                              <div class="col s12">
                              <div class="input-field col s3">
                                 <input type="text" class="datepicker" id="dob">
                                    <label for="dob">Delivery Date</label>
                                 </div>
                                 <div class="input-field col s3">
                                 <input placeholder="24 hours time" id="time-demo2" type="text" class="">
                                    <label for="time-demo2">Time</label>
                                 </div>
                                 <div class="input-field col s6">
                                    <select>
                                       <option value="" disabled selected>Choose your Category</option>
                                       <option value="1">Salad</option>
                                       <option value="2">Ice Tea</option>
                                       <option value="3">Lemonade</option>
                                    </select>
                                    <label>Select Category</label>
                                 </div>
                                 <div class="input-field col s12">
                                    <input type="text" class="datepicker" id="dob">
                                    <label for="dob">Shipping Address Details</label>
                                 </div>
                                 <div class="input-field col s12">
                                    <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                                    <label for="icon_prefix2">Instruction For Delivery Driver</label>
                                 </div>
                                
                                 <div class="input-field col s12" style="background-color: #e4c2c2; border: solid #ed4242 0.5px;">
                                    <p style="font-size: 18px; color:black; padding:10px;">
                                       I Understant this food order is not confirmed until get a call from Food Hawk. Call us if you have any
                                    questions 512-909-6161.
</p> 
                                 </div>
                              <div class="input-field col s12">
                                 <button class="btn waves-effect waves-light right" style="background-color:#ed4242;" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
