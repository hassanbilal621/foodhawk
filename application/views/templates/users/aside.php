<!-- BEGIN: SideNav-->

<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
  <div class="brand-sidebar" style="background-color: #a01515">
    <h1 class="logo-wrapper"><a class=" darken-1" href="<?php echo base_url(); ?>users/index"><img class="hide-on-med-and-down" src="<?php echo base_url(); ?>assets/app-assets/images/logo/logo.png" style="width: 200px !important;margin: 12px 0 0px 5px;" alt=" FoodHawk"></span></a><a class="navbar-toggler" href="#"><i class="material-icons" style="color: white;">radio_button_checked</i></a></h1>
  </div>
  <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">
    <li class="navigation-header">
      <a class="navigation-header-text black-text">Dashboard</a>
      <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>users/"><i class="material-icons">computer</i><span style="color: #444444;">Dashboard</span></a>
    </li>
    <li class="navigation-header">
      <a class="navigation-header-text black-text"> Order Management</a>
      <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/orderfood"><i class="material-icons">event_note</i><span style="color: #444444;">Speed Order</span></a>
    </li>

    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/myfavorites"><i class="material-icons">star</i><span style="color: #444444;">My Favorites</span></a>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/orderhistory"><i class="material-icons">history</i><span style="color: #444444;">Order History</span></a>
    </li>

  </ul>


  <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->