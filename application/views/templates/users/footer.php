<!-- BEGIN: Footer-->

<footer class="page-footer footer footer-static footer-dark darken-4 gradient-shadow navbar-border navbar-shadow" style="background-color: #a01515;margin-top: 65px;">
  <div class="footer-copyright">
    <div class="container"><span>&copy; 2020 All rights reserved.</span></div>
  </div>
</footer>

<!-- END: Footer-->



<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/dropify/js/dropify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/dataTables.select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/search.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/customizer.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-file-uploads.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/data-tables.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-media.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/page-users.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/formatter/jquery.formatter.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-select2.js"></script>
<script>
  $('#credit-demo').formatter({
    'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
    'persistent': true
  });
</script>
</body>

</html>