<header class="page-topbar" id="header">
  <div class="navbar navbar-fixed">
    <nav class="darken-4" style="background-color: #a01515">
      <div class="nav-wrapper">
        <?php
        $userid = $this->session->userdata('food_user_id');
        $notificationdata_count = $this->db->query("SELECT * FROM notification WHERE notification_user_id =  $userid AND `status` = 'unread' AND `admin_user` = '0' ");
        $notificationcount = $notificationdata_count->num_rows();
        $notificationdata = $this->db->query("SELECT	*, 	no.* FROM	notification AS `no`WHERE	`status` = 'unread' AND	admin_user = '0' AND	no.notification_user_id = $userid ORDER BY	no.notification_id DESC LIMIT 10");
       
        $notifications = $notificationdata->result_array();
        ?>
        <?php
        $userid = $this->session->userdata('food_user_id');
        $userdata = $this->db->query("SELECT * FROM	users WHERE user_id = $userid");
        $user = $userdata->row_array();
        ?>
        <ul class="navbar-list right">
          <li><a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown"><i class="material-icons">notifications_none<small class="notification-badge"><?php echo $notificationcount ?></small></i></a></li>
          <li style="padding: 15;"><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $user['profile_image']; ?>" alt="avatar"><i></i></span></a></li>
        </ul>
        <?php ?>


        <!-- notifications-dropdown-->
        <ul class="dropdown-content" id="notifications-dropdown">
          <li>
            <h6>NOTIFICATIONS <span>
                <?php if (!isset($notificationcount)) {
                } else { ?><span class="new badge"><?php echo $notificationcount ?></span> <?php } ?></span></h6>
          </li>

          <li class="divider"></li>
          <?php foreach ($notifications as $notification) : ?>
            <li><a class="black-text capitalize" href=" <?php echo base_url(); ?>users/notification"> <?php echo $notification['notification_title']; ?>
                <time class="darken-2 grey-text media-meta right" datetime="2015-06-12T20:50:48+08:00" style=" font-size: 15px;"><?php echo $notification['date_time']; ?></time>
              </a> </li>
          <?php endforeach; ?>
          <?php if(!isset($notificationcount)) { ?>
            <p style="color: black;margin: 0 0 0 25px;font-size: initial;">No Notification Found</p>
          <?php } else { ?> <a href=" <?php echo base_url(); ?>users/notification" style="color: black;float: right;">Show All</a><?php } ?>
        </ul>
        <!-- profile-dropdown-->
        <ul class="dropdown-content" id="profile-dropdown">
          <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/profile"><i class="material-icons" style="color:#ed4242;">person_outline</i> Profile</a></li>
          <li><a class="grey-text text-darken-1" href="<?php echo base_url(); ?>users/logout"><i class="material-icons" style="color:#ed4242;">keyboard_tab</i> Logout</a></li>
        </ul>
      </div>

    </nav>
  </div>
</header>
<!-- 
<div id="modal3" class="modal">
  <div class="modal-content">
  </div>
</div>
<script>
  function loadnotification(notificationid) {
    // var userid = this.id;
    $.ajax({
      type: "GET",
      url: "<?php echo base_url(); ?>admin/ajax_notification_details/" + notificationid,
      success: function(data) {
        $(".modal-content").html(data);
        $('#modal3').modal('open');



      }
    });
  }
</script> -->