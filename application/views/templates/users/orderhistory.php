<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">
                            <span<span style="font-weight: bold;">Order History</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <section class="users-list-wrapper section">
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <!-- datatable start -->
                                <div class="responsive-table">
                                <table id="page-length-option" class=" display">
                                        <thead>
                                            <tr>
                                                <th>Order Id</th>
                                                <th>Date & Time</th>
                                                <th>Guest</th>
                                                <th>Order Status</th>
                                                <th>Add To Favorites </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $s_no = '1';
                                            foreach ($orders as $order) : ?>
                                                <tr>
                                                    <td> <?php echo $order['order_id']; ?></td>
                                                    <td><?php echo $order['date']; ?> & <?php echo $order['time']; ?></td>
                                                    <td><?php echo $order['guest']; ?></td>
                                                    <?php if ($order['order_status'] == 'approved') { ?>
                                                        <td class="chip green lighten-4">Approved </td>
                                                        
                                                    <?php } elseif ($order['order_status'] == 'pendingapproval') { ?>
                                                        <td class="chip lighten-3 orange">Pending Approval</td>

                                                    <?php } elseif ($order['order_status'] == 'rejected') { ?>
                                                        <td class="chip lighten-4 red">Rejected</td>

                                                    <?php } elseif ($order['order_status'] == 'cancelled') { ?>
                                                        <td class="chip grey lighten-3">Cancelled</td>

                                                    <?php } elseif ($order['order_status'] == 'readyfordelivery') { ?>
                                                        <td class="chip lighten-4 orange">Ready For Delivery</td>

                                                    <?php } else { ?>
                                                        <td class="chip green lighten-5">Delivered</td>

                                                    <?php } ?>
                                                    <?php if ($order['favorites'] == 'yes') { ?>
                                                        <td class="center"><a href="<?php echo base_url(); ?>users/removefavorites/<?php echo $order['order_id']; ?>/his"><i class="material-icons">star</i></a></td>

                                                    <?php } else { ?>
                                                        <td class="center"><a href="<?php echo base_url(); ?>users/favorites/<?php echo $order['order_id']; ?>/his"><i class="material-icons">star_border</i></a></td>
                                                    <?php } ?>
                                                </tr>
                                            <?php $s_no++;
                                            endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>