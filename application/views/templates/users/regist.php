<style>
  .dropify-wrapper {

    height: 150px !important;

  }
</style>
<div class="row register-bg">
  <div class="col s12">
    <div class="container">
      <div id="register-page" class="row">
        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 register-card bg-opacity-8">
          <div class="row">
            <div class="input-field col s12 center">
              <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/TEXT COLOR(2).png" alt="" style="width: 75%;margin: -5px 0px -20px 0px;" class="responsive-img valign"></a>
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              <?php echo form_open_multipart('users/register'); ?>
              <div class="row margin">
                <div class="input-field col s12  m12 l6">
                  <input type="text" name="name" required>
                  <label for="">First & Last Name</label>
                </div>

                <div class="input-field col s12 m12 l6">
                  <input type="text" class="validate" id="phone-code" name="mobile" required>
                  <label for="">Mobile No</label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12  m12 l6">
                  <input type="email" class="validate" name="email" required>
                  <label for="">E Mail</label>
                </div>

                <div class="input-field col s12  m12 l6">
                  <input type="password" name="password" required>
                  <label for="">Password</label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12 m12 l12">
                  <input type="text" name="address" required>
                  <label for="">Address</label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12 m12 l6">
                  <label for="" class="active">Select Country</label>
                  <select name="country" class="select2  browser-default">
                    <?php foreach ($countries as $country) : ?>
                      <option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                    <?php endforeach; ?>

                  </select>
                </div>

                <div class="input-field col s12 m12 l6">
                <label for="" class="active">Select City</label>
                                 <select name="city" class="select2  browser-default">
                                    <?php foreach ($cities as $city) : ?>
                                       <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12 m12 l6">
                <label for="" class="active">Select Zip Code</label>
                                 <select name="zipcode" class="select2  browser-default">
                                    <?php foreach ($zipcodes as $zipcode) : ?>
                                       <option value="<?php echo $zipcode['zipcode']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                </div>
                <div class="input-field col s12 m12 l6">
                  <input type="text" id="credit-demo" name="credit" required>
                  <label for="">credit Card</label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12 m12 l12">
                  <div id="view-file-input">

                    <div class="file-field input-field">
                      <div class="btn">
                        <span>Upload Your Corporate Application</span>
                        <input type="file" name="userfile"  accept="image/png,image/jpg,image/jpeg,application/pdf" required>
                      </div>
                      <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                      </div>
                      <span>Upload only PDF and JPG Images</span>
                    </div>
                  </div>
                  <div class="row">
                    <div class=" col s12">
                      <button class="btn waves-effect waves-light border-round submit col s12">Register Now</button>
                    </div>
                  </div>
                </div>
              </div>
              <?php echo form_close(); ?>
              <div class="row">
                <div class="input-field col s12">
                  <p class="margin medium-small"><a href="<?php echo base_url(); ?>users/login">Already have an account? Login</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<script src="<?php echo base_url(); ?>assets/app-assets/vendors/formatter/jquery.formatter.js"></script>










<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/dropify/js/dropify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/search.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/customizer.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-file-uploads.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-media.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/page-users.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/formatter/jquery.formatter.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-select2.js"></script>
<script>
  $('#credit-demo').formatter({
    'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}',
    'persistent': true
  });
</script>
</body>

</html>